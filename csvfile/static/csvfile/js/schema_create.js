$(document).ready(function() {
  var formCount = $('form').length;
  var addButton = $('#add-column');
  var formContainer = $('#column-forms');
  var emptyForm = $('#empty-column-form').html();

  // Add new formset
  addButton.on('click', function(event) {
    event.preventDefault();
    var newForm = emptyForm.replace(/__prefix__/g, formCount);
    $.ajax({
      url: '/add_column/',
      type: 'POST',
      data: { form_html: newForm },
      dataType: 'html',
      success: function(response) {
        formContainer.append(response);
        formCount++;
      }
    });
  });

  // Remove formset
  formContainer.on('click', '.remove-column', function(event) {
    event.preventDefault();
    var form = $(this).parents('form');
    var formIdx = form.attr('data-formset-index');
    $.ajax({
      url: '/remove_column/',
      type: 'POST',
      data: { form_idx: formIdx },
      dataType: 'html',
      success: function(response) {
        form.remove();
        formContainer.html(response);
      }
    });
  });
});