from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('schema_create', CreateDataSchemaView.as_view(), name='schema_create'),
    path('', userlogin, name='login'),
    path('logout', userlogout, name='logout'),
    path('schema_list', DataSchemaListView.as_view(), name='schema_list'),
    path('schema_detail/<int:pk>', SchemaDetailView.as_view(), name='schema_detail'),
    path('schema_edit/<int:pk>', SchemaUpdateView.as_view(), name='schema_edit'),
    path('create/add_column/', add_column, name='add_column'),
    path('create/remove_column/', remove_column, name='remove_column'),
    path('generate-data/<int:pk>', GenerateDataView.as_view(), name='generate_data'),

]