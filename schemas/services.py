import csv
import os
from faker import Faker
from io import StringIO
from faker import Faker
import random
import csv
import io


def generate_data(schema, num_records):
    fake = Faker()
    columns = schema.columns.all()
    buffer = StringIO()
    writer = csv.writer(buffer)
    header_row = [column.name for column in columns]
    writer.writerow(header_row)
    for i in range(num_records):
        data_row = [getattr(fake, column.type.lower())() for column in columns]
        writer.writerow(data_row)

    file_name = f"{schema.name}_generated.csv"
    file_path = os.path.join('media', file_name)

    with open(file_path, 'w') as file:
        file.write(buffer.getvalue())

    return file_path
