from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.forms import modelformset_factory, formset_factory
from django.http import Http404, JsonResponse, HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import ListView, CreateView, DeleteView, DetailView, UpdateView
from .forms import *
from .models import *
from django.shortcuts import render, redirect, get_object_or_404
from .forms import UserLoginForms
from django.contrib.auth import login, logout
from .services import *


def userlogin(request):
    if request.method == 'POST':
        form = UserLoginForms(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('schema_list')
    else:
        form = UserLoginForms()
    return render(request, 'login.html', {'form': form})


def userlogout(request):
    logout(request)
    return redirect('login/')


class DataSchemaListView(LoginRequiredMixin, ListView):
    model = DataSchema
    template_name = 'schema_list.html'
    context_object_name = 'schemas'

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class SchemaDetailView(LoginRequiredMixin, DetailView):
    model = DataSchema
    template_name = 'schema_detail.html'
    context_object_name = 'schema'
    login_url = '/login/' # Update this to your login URL

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset


class CreateDataSchemaView(CreateView):
    model = DataSchema
    form_class = DataSchemaForm
    template_name = 'schema_create.html'
    success_url = reverse_lazy('schema_list')

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = ColumnFormSet()
        return render(request, self.template_name, {'form': form, 'formset': formset})

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = ColumnFormSet(request.POST)
        if form.is_valid() and formset.is_valid():
            # Save the DataSchema instance
            schema = form.save(commit=False)
            schema.user = request.user
            schema.save()
            # Save the Column instances
            for form in formset:
                if form.is_valid():
                    form.instance.schema = schema
                    form.save()
            return super().form_valid(form)
        else:
            return render(request, self.template_name, {'form': form, 'formset': formset})


def add_column(request):
    ColumnFormSet = formset_factory(ColumnForm, extra=1)
    formset = ColumnFormSet(prefix='columns')
    return render(request, 'add_column.html', {'formset': formset})


def remove_column(request):
    if request.method == 'POST':
        form_idx = int(request.POST.get('form_idx'))
        response_data = {}
        if form_idx >= 0:
            response_data['form_idx'] = form_idx
        return JsonResponse(response_data)
    else:
        return JsonResponse({'error': 'Invalid request method'})


class SchemaUpdateView(LoginRequiredMixin, UpdateView):
    model = DataSchema
    form_class = DataSchemaForm
    template_name = 'schema_edit.html'
    success_url = reverse_lazy('schema_list')
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['formset'] = ColumnFormSet(self.request.POST, instance=self.object)
        else:
            data['formset'] = ColumnFormSet(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        formset = context['formset']
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.user = self.request.user
            self.object.save()
            if formset.is_valid():
                formset.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        if 'add_column' in request.POST:
            formset = self.get_context_data()['formset']
            formset.extra += 1
            return self.render_to_response(self.get_context_data())

        if 'delete_column' in request.POST:
            column_id = request.POST.get('delete_column')
            column = get_object_or_404(Column, id=column_id)
            column.delete()
            return redirect('schema_update', pk=self.object.pk)

        return super().post(request, *args, **kwargs)


class DeleteDataSchemaView(DeleteView):
    model = DataSchema
    success_url = reverse_lazy('schema_list')
    template_name = 'schema_delete.html'

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if obj.user != self.request.user:
            raise Http404("You don't have permission to access this object")
        return obj


class GenerateDataView(LoginRequiredMixin, View):
    def post(self, request, pk):
        schema = get_object_or_404(DataSchema, pk=pk)
        num_records = int(request.POST.get('num_records', 0))
        if num_records <= 0:
            messages.error(request, 'Please enter a valid number of records.')
            return redirect('schema_detail', pk=schema.pk)
        file_path = generate_data(schema, num_records)
        GeneratedData.objects.create(
            schema=schema,
            num_records=num_records,
            filepath=file_path,
            user=request.user,
            status='completed'
        )
        return redirect('schema_detail', pk=schema.pk)