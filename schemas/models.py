from django.db import models
from django.contrib.auth.models import User


class DataSchema(models.Model):
    STRING_QUOTE_CHOICES = (
        ('"', 'Double Quote'),
        ("'", 'Single Quote'),
    )

    SEPARATOR_CHOICES = (
        (',', 'Comma'),
        (';', 'Semicolon'),
        (':', 'Colon'),
        ('\t', 'Tab'),
    )
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    create_at = models.DateField(auto_now_add=True)
    separator = models.CharField(max_length=2, choices=SEPARATOR_CHOICES, default=',')
    string_quote = models.CharField(max_length=1, choices=STRING_QUOTE_CHOICES, default='"')

    def __str__(self):
        return self.name


class Column(models.Model):

    COLUMN_TYPES = (
        ('full_name', 'Full Name'),
        ('job', 'Job'),
        ('email', 'Email'),
        ('domain_name', 'Domain Name'),
        ('phone_number', 'Phone Number'),
        ('company_name', 'Company Name'),
        ('text', 'Text'),
        ('integer', 'Integer'),
        ('address', 'Address'),
        ('date', 'Date')
    )

    schema = models.ForeignKey(DataSchema, on_delete=models.CASCADE, related_name='columns')
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=15, choices=COLUMN_TYPES, default='full_name')
    order = models.IntegerField()
    range_min = models.IntegerField(null=True, blank=True)
    range_max = models.IntegerField(null=True, blank=True)


class GeneratedData(models.Model):
    STATUS_CHOICES = (
        ('pending', 'Pending'),
        ('completed', 'Completed'),
        ('failed', 'Failed'),
    )

    schema = models.ForeignKey(DataSchema, on_delete=models.CASCADE)
    num_records = models.PositiveIntegerField()
    filepath = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='pending')

    def __str__(self):
        return f"{self.schema.name} ({self.num_records} records) - {self.created_at.strftime('%Y-%m-%d %H:%M:%S')}"

    def get_status_display(self):
        return dict(self.STATUS_CHOICES)[self.status]
