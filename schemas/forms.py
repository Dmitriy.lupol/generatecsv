from django import forms
from django.core.exceptions import ValidationError
from django.forms import inlineformset_factory
from .models import DataSchema, Column
from django.contrib.auth.forms import AuthenticationForm


class UserLoginForms(AuthenticationForm):
    username = forms.CharField(label='', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))


class DataSchemaForm(forms.ModelForm):
    name = forms.CharField(label='Name')
    separator = forms.ChoiceField(choices=DataSchema.SEPARATOR_CHOICES, label='Column separator')
    string_quote = forms.ChoiceField(choices=DataSchema.STRING_QUOTE_CHOICES, label='String character')

    class Meta:
        model = DataSchema
        fields = ['name', 'separator', 'string_quote']

    def __init__(self, *args, user=None, **kwargs):
        super(DataSchemaForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        self.fields['separator'].widget.attrs.update({'class': 'form-control'})
        self.fields['string_quote'].widget.attrs.update({'class': 'form-control'})
        self.instance.user = user


# class ColumnForm(forms.ModelForm):
#     class Meta:
#         model = Column
#         exclude = ['schema']
#         fields = ['name', 'type', 'range_min', 'range_max', 'order']
#         widgets = {
#             'range_min': forms.HiddenInput(),
#             'range_max': forms.HiddenInput(),
#         }
#
#     def clean(self):
#         cleaned_data = super().clean()
#         type = cleaned_data.get('type')
#         range_min = cleaned_data.get('range_min')
#         range_max = cleaned_data.get('range_max')
#
#         if type == Column.COLUMN_TYPES[1][0]:  # 'Integer'
#             if range_min is None or range_max is None:
#                 raise ValidationError("Range is required for Integer columns")
#             if range_min >= range_max:
#                 raise ValidationError("Range minimum must be less than range maximum")
#         else:
#             cleaned_data['range_min'] = None
#             cleaned_data['range_max'] = None
#
#         return cleaned_data

class ColumnForm(forms.ModelForm):
    # name = forms.CharField(label='Schema columns')
    class Meta:
        model = Column
        fields = ['name', 'type', 'range_min', 'range_max', 'order']

    def __init__(self, *args, **kwargs):
        super(ColumnForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        self.fields['type'].widget.attrs.update({'class': 'form-control'})
        # self.fields['range_min'].widget.attrs.update({'class': 'form-control'})
        # self.fields['range_max'].widget.attrs.update({'class': 'form-control'})
        self.fields['order'].widget.attrs.update({'class': 'form-control'})

        self.fields['range_min'].widget = forms.HiddenInput()
        self.fields['range_max'].widget = forms.HiddenInput()

        def clean(self):
            cleaned_data = super().clean()
            type = cleaned_data.get('type')
            range_min = cleaned_data.get('range_min')
            range_max = cleaned_data.get('range_max')

            if type == Column.COLUMN_TYPES[1][0]:  # 'Integer'
                if range_min is None or range_max is None:
                    raise ValidationError("Range is required for Integer columns")
                if range_min >= range_max:
                    raise ValidationError("Range minimum must be less than range maximum")
            else:
                cleaned_data['range_min'] = None
                cleaned_data['range_max'] = None

            return cleaned_data


ColumnFormSet = inlineformset_factory(DataSchema, Column, form=ColumnForm, extra=1, can_delete=True)


class NumRowsForm(forms.Form):
    num_rows = forms.IntegerField(label='Number of Rows', min_value=1, max_value=100000)


class DataGenerationForm(forms.Form):
    schema = forms.ModelChoiceField(queryset=DataSchema.objects.all())
    num_records = forms.IntegerField(min_value=1)

